package util.dto;

/**
 * Esta excepción es lanzada hacia el controlador cuando se produce una incidencia en el API
 *
 * @author Juan Pablo Canepa Alvarez
 *
 */
public class ApiException extends Exception {

	private static final long serialVersionUID = -7821958228183810886L;

	private final String code;
	private final String message;
	private final String detailMessage;

	private ApiException(String message) {
		this(message, null);
	}

	private ApiException(String code, String message) {
		this(code, message, null);
	}

	private ApiException(String code, String message, String detailMessage) {
		this.code = code;
		this.message = message;
		this.detailMessage = detailMessage;
	}

	public static ApiException of(String message) {
		return new ApiException(message);
	}

	public static ApiException of(String code, String message) {
		return new ApiException(code, message);
	}

	public static ApiException of(String code, String message, String detailMessage) {
		return new ApiException(code, message, detailMessage);
	}

	public String getCode() {
		return code;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public String getDetailMessage() {
		return detailMessage;
	}

}
